﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using UnityEngine.UI;

public class AdsManager : MonoBehaviour {

    public string appIDKita             = "ca-app-pub-xxxxxxxxxxxxxxxx~yyyyyyyyyy";
    public string adUnitIDBannerBawah   = "ca-app-pub-xxxxxxxxxxxxxxxx~yyyyyyyyyy";
    public string adUnitIDRewardedVideo = "ca-app-pub-xxxxxxxxxxxxxxxx~yyyyyyyyyy";
    public string adUnitIDInterstitial  = "ca-app-pub-xxxxxxxxxxxxxxxx~yyyyyyyyyy";
    public string adUnitIDSaveHighscore = "ca-app-pub-xxxxxxxxxxxxxxxx~yyyyyyyyyy";




    public bool rewardnyawa = false;
    public bool rewardsavehs = false;
    public AdRequest requestVideo;
    public AdRequest requestVideo2;

    private GameVariables gamevarscript;
    private CountdownIklanHandler handlervideoad;
    public AudioHandler audioscript;
    public GameEventManager eventscr;

    public BannerView bannerView;
    private RewardBasedVideoAd rewardBasedVideo;
    private RewardBasedVideoAd saveScoreVideo;
    InterstitialAd interstitial;

    public void Start()
    {
        gamevarscript = transform.GetComponent(typeof(GameVariables)) as GameVariables;
        handlervideoad = transform.GetComponent(typeof(CountdownIklanHandler)) as CountdownIklanHandler;
        InitializeAds();
        this.RequestInterstitialAds();
        this.RequestSaveHighScoreVideo();
        this.RequestRewardedVideo();
        this.rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;

    }

    public void InitializeAds()
    {
        MobileAds.Initialize(appIDKita);
    }

    //------------------------------------------------------Banner------------------------------------------------------//
    public void RequestBanner()
    {
        //320x50 
        bannerView = new BannerView(adUnitIDBannerBawah, AdSize.Banner, AdPosition.Top);
        AdRequest request = new AdRequest.Builder().Build();
        bannerView.LoadAd(request);
    }

    public void BannerViewDestroy()
    {
        bannerView.Destroy();
    }

    //------------------------------------------------------VIDEO------------------------------------------------------//

    public void RequestRewardedVideo()
    {
        this.rewardBasedVideo = RewardBasedVideoAd.Instance;
        requestVideo = new AdRequest.Builder().Build();
        rewardBasedVideo.LoadAd(requestVideo, adUnitIDRewardedVideo);
    }

    public void ShowRewardedVideo(){
        if (rewardBasedVideo.IsLoaded())
        {
            rewardnyawa = true;
            rewardsavehs = false;
            rewardBasedVideo.Show();
        }
        if (!rewardBasedVideo.IsLoaded())
        {
            rewardBasedVideo.LoadAd(requestVideo, adUnitIDRewardedVideo);
        }
    }

    public void RequestSaveHighScoreVideo()
    {
        this.saveScoreVideo = RewardBasedVideoAd.Instance;
        requestVideo2 = new AdRequest.Builder().Build();
        saveScoreVideo.LoadAd(requestVideo2, adUnitIDSaveHighscore);
    }

    public void ShowSaveHighScoreVideo()
    {
        if (rewardBasedVideo.IsLoaded())
        {
            rewardnyawa = false;
            rewardsavehs = true;
            rewardBasedVideo.Show();
        }
        if (!rewardBasedVideo.IsLoaded())
        {
            rewardBasedVideo.LoadAd(requestVideo, adUnitIDSaveHighscore);
        }
    }
  
    public void HandleRewardBasedVideoRewarded (object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        print("User rewarded with: " + amount.ToString() + " " + type);

        if (rewardnyawa == true)
        {
            handlervideoad.afterads();
            rewardnyawa = false;
            Debug.Log("nyawaditambah");
        }
        if (rewardsavehs == true)
        {
            gamevarscript.Savescore();
            rewardsavehs = false;
            Debug.Log("highscoredisimpan");
        }
    }


    //------------------------------------------------------Interstitial------------------------------------------------------//

    public IEnumerator ShowInterstitialAd(float waittime)
    {
        if (!interstitial.IsLoaded())
        {
            RequestInterstitialAds();
        }
        yield return new WaitForSeconds(waittime);
        //Show Ad
        if (interstitial.IsLoaded())
        {
           // eventscr.OnGamePaused();
            interstitial.Show();
            audioscript.StopAllSound();

            Debug.Log("SHOW AD XXX");
        }


    }


    public void RequestInterstitialAds()
    {

        interstitial = new InterstitialAd(adUnitIDInterstitial);

       // //***Test***
       // AdRequest request = new AdRequest.Builder()
       //.AddTestDevice(AdRequest.TestDeviceSimulator)       // Simulator.
       //.AddTestDevice("2077ef9a63d2b398840261c8221a0c9b")  // My test device.
       //.Build();

        //***Production***
        AdRequest request = new AdRequest.Builder().Build();

        //Register Ad Close Event
        interstitial.OnAdClosed += Interstitial_OnAdClosed;

        // Load the interstitial with the request.
        interstitial.LoadAd(request);

        Debug.Log("AD LOADED XXX");

    }

    //Ad Close Event
    private void Interstitial_OnAdClosed(object sender, System.EventArgs e)
    {
        eventscr.startTheGame();
    }

    private IEnumerator ForceDestroyBanner(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        BannerViewDestroy();
    }
}
