﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHandler : MonoBehaviour {
	
	//BGM
	public AudioClip mainBGM;

	private AudioSource source;

	// Use this for initialization
	void Awake () {
		source = GetComponent<AudioSource>();
	}

	void Start (){
		source.clip = mainBGM;
		source.Play ();
		source.loop = true;
	}

	public void PlayAudio(AudioClip audio){
		source.PlayOneShot (audio);
	}

    public void StopAllSound (){
        source.Stop();
    }

    public void ResumeAllSound(){
        source.clip = mainBGM;
        source.Play();
        source.loop = true;
    }
}
