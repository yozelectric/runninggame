﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallShopManager : MonoBehaviour {

	public Transform[] balls;
	public float deltaX;

	[Header("Canvas Props")]
	public Transform ballSkinContent;
	public Sprite coinSprite;
	public Sprite diamondSprite;
	public Transform confirmImage;
	public Text confirmPrice;
	public Text afterPurchaseText;
	public Canvas canvas;
	public Transform buyWithCoinButton;
	public Transform buyWithDiamondButton;
	public Text coinPriceText;
	public Text diamondPriceText;
	public Transform equipButton;
	public RectTransform coinLayout;
	public RectTransform diamondLayout;
	public RectTransform confirmLayout;
	public Transform equipped;
	public Transform objekBola;
	public Text coinText;
	public Text diamondText;

	[Header("Temp Variables")]
	public int tempPriceCoin;
	public int tempPriceDiamond;
	public int tempBallIndex;
	public int tempPurchaseType;
	public string tempAfterPurchaseMessage;

	private Vector3 newPos;
	private float svRectWidth;
	private Transform[] instBall;
	private GameVariables gamevarscript;
	private enum PurchaseType{ COIN, DIAMOND };
	private PurchaseType purchaseType;

	void Start(){
		gamevarscript = GameObject.FindGameObjectWithTag("Spawner").GetComponent(typeof(GameVariables)) as GameVariables;
		instBall = new Transform[balls.Length];
		for (int i = 0; i < balls.Length; i++) {
			newPos = new Vector3 (transform.position.x + (i * deltaX), transform.position.y, transform.position.z);
			instBall [i] = Instantiate (balls [i], newPos, balls[i].transform.rotation, ballSkinContent);
			instBall [i].GetComponent<BallVariables> ().index = i;

			if (i == gamevarscript.currentSkinIndex) {
				Debug.Log (gamevarscript.currentSkinIndex);
				objekBola.GetComponent<MeshFilter> ().mesh = instBall [i].GetComponent<MeshFilter> ().mesh;
				objekBola.localScale = instBall [i].transform.localScale * 2;
				objekBola.GetComponent<MeshRenderer> ().materials = instBall [i].GetComponent<MeshRenderer> ().materials;
				objekBola.localRotation = instBall [i].transform.localRotation;
				if (instBall [i].GetComponent<BallVariables> ().useColorMod) {
					objekBola.GetComponent<MeshRenderer> ().material.SetColor ("_Color", instBall [i].GetComponent<BallVariables> ().color);
				}
				equipped.gameObject.SetActive (true);
				equipButton.gameObject.SetActive (false);
				buyWithCoinButton.gameObject.SetActive (false);
				buyWithDiamondButton.gameObject.SetActive (false);
				ballSkinContent.transform.localPosition = new Vector3(i * -157.44f, ballSkinContent.transform.position.y, ballSkinContent.transform.position.z);
				Debug.Log (i * -157.44f);
			}
		}
		svRectWidth = transform.parent.GetComponent<RectTransform> ().rect.width * canvas.scaleFactor;

		transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(svRectWidth + (157.44f * (balls.Length-1)), 0);
		Debug.Log (transform.parent.name);
	}

	public void ConfirmPurchaseCoin(){
		purchaseType = PurchaseType.COIN;
		confirmImage.GetComponent<Image> ().sprite = coinSprite;
		confirmPrice.text = tempPriceCoin.ToString ();
		Debug.Log (purchaseType);
	}

	public void ConfirmPurchaseDiamond(){
		purchaseType = PurchaseType.DIAMOND;
		confirmImage.GetComponent<Image> ().sprite = diamondSprite;
		confirmPrice.text = tempPriceDiamond.ToString ();
		Debug.Log (purchaseType);
	}

	public void ContinuePurchase(){
		int currentSkinAvailability;

		if (purchaseType == PurchaseType.COIN) {
			int currentCoins;
			if (PlayerPrefs.HasKey ("JumlahKoin")) {
				currentCoins = PlayerPrefs.GetInt ("JumlahKoin");
				if (currentCoins < tempPriceCoin) {
					tempAfterPurchaseMessage = "Not Enough Coins";
				} else {
					tempAfterPurchaseMessage = "Purchase Completed";
					currentCoins -= tempPriceCoin;
					PlayerPrefs.SetInt ("JumlahKoin", currentCoins);
					Debug.Log (currentCoins);

					if (PlayerPrefs.HasKey ("SkinAvailability")) {
						currentSkinAvailability = PlayerPrefs.GetInt ("SkinAvailability");
						currentSkinAvailability += (int)Mathf.Pow (2, tempBallIndex);
						PlayerPrefs.SetInt ("SkinAvailability", currentSkinAvailability);
						gamevarscript.skinAvailability = currentSkinAvailability;
					}

					PlayerPrefs.Save ();
				}

				afterPurchaseText.text = tempAfterPurchaseMessage;
			}
		} else {
			int currentDiamond;
			if (PlayerPrefs.HasKey ("JumlahDiamond")) {
				currentDiamond = PlayerPrefs.GetInt ("JumlahDiamond");
				if (currentDiamond < tempPriceDiamond) {
					tempAfterPurchaseMessage = "Not Enough Diamonds";
				} else {
					tempAfterPurchaseMessage = "Purchase Completed";
					currentDiamond -= tempPriceDiamond;
					PlayerPrefs.SetInt ("JumlahDiamond", currentDiamond);
					Debug.Log (currentDiamond);

					if (PlayerPrefs.HasKey ("SkinAvailability")) {
						currentSkinAvailability = PlayerPrefs.GetInt ("SkinAvailability");
						currentSkinAvailability += (int)Mathf.Pow (2, tempBallIndex);
						PlayerPrefs.SetInt ("SkinAvailability", currentSkinAvailability);
						gamevarscript.skinAvailability = currentSkinAvailability;
					}

					PlayerPrefs.Save ();
				}

				afterPurchaseText.text = tempAfterPurchaseMessage;
			}
		}
	}

	public void DisableBalls(){
		for (int i = 0; i < instBall.Length; i++) {
			instBall [i].gameObject.SetActive (false);
		}
	}

	public void EnableBalls(){
		for (int i = 0; i < instBall.Length; i++) {
			instBall [i].gameObject.SetActive (true);
			if (i == tempBallIndex) {
				instBall [i].GetComponent<BallVariables> ().CheckBall ();
			}
		}
	}

	public void UpdateCurrency(){
		coinText.text = PlayerPrefs.GetInt ("JumlahKoin").ToString ();
		diamondText.text = PlayerPrefs.GetInt ("JumlahDiamond").ToString ();
	}

	public void EquipBall(){
		if (PlayerPrefs.HasKey ("CurrentSkin")) {
			PlayerPrefs.SetInt ("CurrentSkin", tempBallIndex);
			PlayerPrefs.Save ();
			gamevarscript.currentSkinIndex = PlayerPrefs.GetInt ("CurrentSkin");
		}
		for (int i = 0; i < instBall.Length; i++) {
			if (i == tempBallIndex) {
				instBall [i].GetComponent<BallVariables> ().CheckBall ();
			}
		}
	}

	public void ForceRebuildConfirmLayout(){
		LayoutRebuilder.ForceRebuildLayoutImmediate (confirmLayout);
	}
}
