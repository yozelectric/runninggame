﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallVariables : MonoBehaviour {

	[Header("Canvas Props")]
	public Color color;
	public bool useColorMod;
	public int index;

	[Header("Detail Harga")]
	public bool canUseCoin;
	public int hargaCoin;
	public bool canUseDiamond;
	public int hargaDiamond;

	private MeshRenderer rend;
	private GameVariables gamevarscript;
	private BallShopManager ballShopScript;
	private float startTime;
	private float focusSpeed;

	void Awake(){
		ballShopScript = GameObject.FindGameObjectWithTag("BallShopManager").GetComponent(typeof(BallShopManager)) as BallShopManager;
		gamevarscript = GameObject.FindGameObjectWithTag("Spawner").GetComponent(typeof(GameVariables)) as GameVariables;
		if (useColorMod) {
			rend = GetComponent<MeshRenderer> ();
			rend.material.SetColor ("_Color", color);
		}
	}

	void OnMouseDown(){
		CheckBall();
	}

	/*
	 * MASIH TBD, BISA DI CONSIDER BUAT DIPAKE
	IEnumerator FocusToBall(){
		float elapsedTime = 0;
		float focusTime = 0.5f;
		Vector3 targetFocus = new Vector3 (index * -157.44f, ballShopScript.ballSkinContent.transform.position.y, ballShopScript.ballSkinContent.transform.position.z);
		//float step = (Time.time - startTime) * focusSpeed / Vector3.Distance (ballShopScript.ballSkinContent.transform.localPosition, targetFocus);
		//ballShopScript.ballSkinContent.transform.localPosition = Vector3.Lerp (ballShopScript.ballSkinContent.transform.localPosition, targetFocus, step);
		//while (ballShopScript.ballSkinContent.transform.localPosition != targetFocus) {
		//	ballShopScript.ballSkinContent.transform.localPosition += new Vector3 (index * -157.44f / 1000f, ballShopScript.ballSkinContent.transform.position.y, ballShopScript.ballSkinContent.transform.position.z);
		//	new WaitForSeconds (0.001f);
		//}

		while (elapsedTime < focusTime){
			ballShopScript.ballSkinContent.transform.localPosition = Vector3.Lerp (ballShopScript.ballSkinContent.transform.localPosition, targetFocus, elapsedTime/focusTime);
			elapsedTime += Time.deltaTime;
			yield return new WaitForEndOfFrame ();
		}
		yield break;
	}
	*/

	public void CheckBall(){
		//StartCoroutine (FocusToBall ());
		ballShopScript.objekBola.GetComponent<MeshFilter> ().mesh = GetComponent<MeshFilter> ().mesh;
		ballShopScript.objekBola.localScale = transform.localScale * 2;
		ballShopScript.objekBola.GetComponent<MeshRenderer> ().materials = GetComponent<MeshRenderer> ().materials;
		ballShopScript.objekBola.localRotation = transform.localRotation;
		if (useColorMod) {
			ballShopScript.objekBola.GetComponent<MeshRenderer> ().material.SetColor ("_Color", color);
		} else {
			//ballShopScript.objekBola.GetComponent<MeshRenderer> ().material.SetColor ("_Color", GetComponent<MeshRenderer> ().material.color);
		}
		if ((gamevarscript.skinAvailability & 1 << (index)) == 1 << (index)) {
			if (index == gamevarscript.currentSkinIndex) {
				ballShopScript.equipped.gameObject.SetActive (true);
				ballShopScript.equipButton.gameObject.SetActive (false);
				ballShopScript.buyWithCoinButton.gameObject.SetActive (false);
				ballShopScript.buyWithDiamondButton.gameObject.SetActive (false);
			} else {
				ballShopScript.tempBallIndex = index;
				ballShopScript.equipButton.gameObject.SetActive (true);
				ballShopScript.equipped.gameObject.SetActive (false);
				ballShopScript.buyWithCoinButton.gameObject.SetActive (false);
				ballShopScript.buyWithDiamondButton.gameObject.SetActive (false);
			}
		} else {
			ballShopScript.equipButton.gameObject.SetActive (false);
			ballShopScript.equipped.gameObject.SetActive (false);
			ballShopScript.tempPriceCoin = hargaCoin;
			ballShopScript.tempPriceDiamond = hargaDiamond;
			ballShopScript.tempBallIndex = index;
			if (canUseCoin && canUseDiamond) {
				ballShopScript.buyWithCoinButton.gameObject.SetActive (true);
				ballShopScript.coinPriceText.text = hargaCoin.ToString ();
				ballShopScript.buyWithDiamondButton.gameObject.SetActive (true);
				ballShopScript.diamondPriceText.text = hargaDiamond.ToString ();
			} else if (canUseCoin) {
				ballShopScript.buyWithCoinButton.gameObject.SetActive (true);
				ballShopScript.buyWithDiamondButton.gameObject.SetActive (false);
				ballShopScript.coinPriceText.text = hargaCoin.ToString ();
			} else if (canUseDiamond) {
				ballShopScript.buyWithCoinButton.gameObject.SetActive (false);
				ballShopScript.buyWithDiamondButton.gameObject.SetActive (true);
				ballShopScript.diamondPriceText.text = hargaDiamond.ToString ();
			}
			LayoutRebuilder.ForceRebuildLayoutImmediate (ballShopScript.coinLayout);
			LayoutRebuilder.ForceRebuildLayoutImmediate (ballShopScript.diamondLayout);
		}
	}
}
