﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownIklanHandler : MonoBehaviour {

    private GameVariables gamevarscript;

    public Canvas canvasTrigger;

    private int min;
    private int sec;

    public GameObject tombolPlay;
    public GameObject teksCountDownGameOver;

    public float waktuIklan;

	// Use this for initialization
	void Start () {
        gamevarscript = transform.GetComponent(typeof(GameVariables)) as GameVariables;

        if (PlayerPrefs.HasKey("WaktuIklan"))
        {
            LoadWaktuIklan();
        }

        if (!PlayerPrefs.HasKey("WaktuIklan"))
        {
            waktuIklan = 120;
        }

        //if (gamevarscript.Nyawa <= 0)
        //{
        //    // timeLeft = 1;
        //    if (!tombolPlay.activeInHierarchy)
        //    {
        //        tombolPlay.SetActive(true);
        //        teksCountDownGameOver.transform.parent.gameObject.SetActive(false);
        //    }
        //}

	}
	
	// Update is called once per frame
	void Update () {
        //khusus untuk watch video tombol restart
        if (canvasTrigger.gameObject.activeInHierarchy)
        {
            if (gamevarscript.Nyawa <= 0)
            {
                if (waktuIklan > 0.9f)
                {
                    if (tombolPlay.activeInHierarchy)
                    {
                        tombolPlay.SetActive(false);
                        teksCountDownGameOver.transform.parent.gameObject.SetActive(true);
                    }

                    waktuIklan -= Time.deltaTime;
                    min = Mathf.FloorToInt(waktuIklan / 60);
                    sec = Mathf.FloorToInt(waktuIklan % 60);
                    teksCountDownGameOver.GetComponent<Text>().text = "Wait " + min.ToString("00") + ":" + sec.ToString("00");

                }

                else if (waktuIklan <= 0.9f)
                {

                    gamevarscript.Nyawa = 5;
                    gamevarscript.SaveNyawa();
                    if (!tombolPlay.activeInHierarchy)
                    {
                        tombolPlay.SetActive(true);
                        teksCountDownGameOver.transform.parent.gameObject.SetActive(false);
                    }
                    waktuIklan = 120;
                    SaveWaktuIklan();
                }
            }
            else
            {
                // timeLeft = 1;
                if (!tombolPlay.activeInHierarchy)
                {
                    tombolPlay.SetActive(true);
                    teksCountDownGameOver.transform.parent.gameObject.SetActive(false);
                }
            }

        }



	}



    public void SaveWaktuIklan()
    {
        PlayerPrefs.SetFloat("WaktuIklan", waktuIklan);
        PlayerPrefs.Save();
    }
    public void LoadWaktuIklan()
    {
        float waktuiklanloaded = PlayerPrefs.GetFloat("WaktuIklan");
        waktuIklan = waktuiklanloaded;
    }

    public void afterads()
    {
        if (waktuIklan > 1) 
            waktuIklan = 1;

    }
}
