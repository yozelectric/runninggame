﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameEventManager : MonoBehaviour
{

 

    public bool pause;
    public bool gameOver;

    public bool ingame;
    public bool mainmenu;

    public Canvas gameOverCanvas;
    public Canvas inGameCanvas;
    public Canvas PauseMenuCanvas;

    public GameObject menuPanel;

    public Button pausebutton;
    public Text tekscountdowngame;

    private CountdownIklanHandler handlervideoad;
    private GameVariables gamevarscript;

    private AudioHandler audioScript;
    public AudioClip coinSFX;
    public AudioClip gameOverSFX;

    private AdsManager adsmanager;

    private IEnumerator Coroutine;

    public void Start()
    {
        handlervideoad = transform.GetComponent(typeof(CountdownIklanHandler)) as CountdownIklanHandler;
        gamevarscript = transform.GetComponent(typeof(GameVariables)) as GameVariables;
        audioScript = GameObject.Find("AudioHandler").GetComponent(typeof(AudioHandler)) as AudioHandler;
        adsmanager = transform.GetComponent(typeof(AdsManager)) as AdsManager;
    }

    public void GetCoin()
    {
        audioScript.PlayAudio(coinSFX);
        gamevarscript.CoinsOwned++;
        gamevarscript.teksscorecoin.text = gamevarscript.CoinsOwned.ToString();
    }

    public void startTheGame()
    {
        

        gamevarscript.SaveCoins();
        gamevarscript.SaveNyawa();
        gamevarscript.SaveDiamond();
        if (adsmanager.bannerView != null)
            adsmanager.BannerViewDestroy();
        SceneManager.LoadScene("scenegame", LoadSceneMode.Single);
    }

    public void BackToMainMenu()
    {
       
        gamevarscript.SaveCoins();
        gamevarscript.SaveDiamond();
        gamevarscript.SaveNyawa();
        handlervideoad.SaveWaktuIklan();
        if (adsmanager.bannerView != null)
            adsmanager.BannerViewDestroy();
        SceneManager.LoadScene("mainmenu", LoadSceneMode.Single);
    }

    public void OnReplayButtonPressed(){
            startTheGame();
    }

    public void QuitGame()
    {
        
        gamevarscript.SaveCoins();
        gamevarscript.SaveDiamond();
        gamevarscript.SaveNyawa();
        if (adsmanager.bannerView != null)
            adsmanager.BannerViewDestroy();
        handlervideoad.SaveWaktuIklan();
        Application.Quit();
    }


    public void OnGamePaused()
    {
        if (!gameOver) pause = !pause;
        pausebutton.gameObject.SetActive(false);
        PauseMenuCanvas.gameObject.SetActive(pause);
        if (adsmanager.bannerView == null)
            adsmanager.RequestBanner();


    }

    public void OnGameResumed()
    {
        if (adsmanager.bannerView != null)
            adsmanager.BannerViewDestroy();
        PauseMenuCanvas.gameObject.SetActive(false);
        CountdownPlay();
        if (!gameOver) pause = !pause;
    }

    public void OnGameOver()
    {
        if (gamevarscript.Nyawa == 5)
        {
            Coroutine = adsmanager.ShowInterstitialAd(3.2f);
            StartCoroutine(Coroutine);
        }
        gamevarscript.Nyawa--;
        gamevarscript.teksNyawa.text = gamevarscript.Nyawa.ToString();

        if (gamevarscript.scoretime > gamevarscript.highscore)
        {
            gamevarscript.SaveHighscore();
        }

        if (adsmanager.bannerView == null)
            adsmanager.RequestBanner();
        //if (gamevarscript.Nyawa <= 0)
        //{
        //    adsmanager.RequestRewardedVideo();
        //}
        //if (gamevarscript.Nyawa == 2)
        //{
        //    adsmanager.RequestInterstitialAds();
        //}

        audioScript.GetComponent<AudioSource>().Stop();
        audioScript.PlayAudio(gameOverSFX);

        gameOver = true;
        gameOverCanvas.gameObject.SetActive(true);
        inGameCanvas.gameObject.SetActive(false);

        gamevarscript.LoadHighscore();

        gamevarscript.teksHighscoreGameOver.text = gamevarscript.highscore.ToString("F0") + " m";
        gamevarscript.tekscoingameover.text = gamevarscript.CoinsOwned.ToString();
        gamevarscript.teksdistancegameover.text = gamevarscript.scoretime.ToString("F0") + " m";
       
        gamevarscript.SaveCoins();
    }

    public void CountdownPlay()
    {
        pausebutton.gameObject.SetActive(false);
        tekscountdowngame.gameObject.SetActive(true);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)){
            if (pause){
                OnGameResumed();
            }
            if (gameOver){
                BackToMainMenu();
            }
            if (mainmenu){
                QuitGame();
            }
            if (ingame && !gameOver && !pause){
                BackToMainMenu();

            }
        }
    }







}
