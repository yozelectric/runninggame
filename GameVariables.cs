﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameVariables : MonoBehaviour
{

    private Scene currentScene;
    [Header("In Game")]


    public Text teksscorecoin;
    public Text teksdiamond;
    public Text teksjarak;
    public Text teksNyawa;

    public int CoinsOwned;
    public int jumlahDiamond;
    public float scoretime;
    public int Nyawa;
    public float highscore;



    [Header("Khusus Shop")]
    public Text tekscoinShop;
    public Text teksDiamondShop;

    [Header("Khusus Game Over")]
    public Text tekscoingameover;
    public Text teksdistancegameover;
    public Text teksHighscoreGameOver;

    [Header("Khusus Ultimate")]
    public int slot2Enabled;
    public Text teksSkill1;
    public Text teksSkill2;
    public Text teksDemolish;
    public Text teksInvis;
    public Text teksMagnet;
    public Text teksFly;
    public Image gambarSkill1;
    public Image gambarSkill2;

    [Header("Khusus Bola")]
    public int currentSkinIndex;
    public int skinAvailability;

    [Header("Khusus Achievements")]
    public int unlockedSkenario;
    //public int score10k;
    //public int score25k;
    //public int score50k;
    //public int unlock1;
    //public int unlock2;
    //public int unlock3;
    //public int unlock4;
    //public int unlock5;
    //public int unlockall;

    public GameObject tick10k;
    public GameObject tick25k;
    public GameObject tick50k;
    public GameObject tickfanta;
    public GameObject tickcandy;
    public GameObject tickruins;
    public GameObject tickdeser;
    public GameObject tickocean;
    public GameObject tickall;


    // Use this for initialization
    void Awake()
    {

        //PlayerPrefs.DeleteAll();
        currentScene = SceneManager.GetActiveScene();
        //--------------------------------------------------------------------------------------------------------------//

        if (PlayerPrefs.HasKey("CurrentScore"))
        {
            Loadscore();
            if (teksjarak != null)
            {
                teksjarak.text = scoretime.ToString("F0") + " m";
            }
        }


        //teksdiamond.text = "Your Diamond : " + jumlahDiamond;
        if (PlayerPrefs.HasKey("JumlahKoin"))
        {
            LoadCoins();
            if (teksscorecoin != null)
            {
                teksscorecoin.text = CoinsOwned.ToString();
            }
        }
        if (PlayerPrefs.HasKey("JumlahNyawa"))
        {
            LoadNyawa();
            if (teksNyawa != null)
            {
                teksNyawa.text = Nyawa.ToString();
            }
        }
        if (PlayerPrefs.HasKey("JumlahDiamond"))
        {
            LoadDiamond();
            if (teksdiamond != null)
            {
                teksdiamond.text = jumlahDiamond.ToString();
            }
            //PlayerPrefs.SetInt ("JumlahDiamond", 1000);
            //PlayerPrefs.Save ();
        }
        if (PlayerPrefs.HasKey("JumlahHighscore"))
        {
            LoadHighscore();
            if (teksHighscoreGameOver != null)
            {
                teksHighscoreGameOver.text = highscore.ToString("F0") + " m";
            }
        }

        if (PlayerPrefs.HasKey("Slot2Enabled"))
        {
            if (PlayerPrefs.GetInt("Slot2Enabled") == 1 && PlayerPrefs.HasKey("Slot2Skill"))
            {
                LoadSkill2();
            }
            //PlayerPrefs.SetInt ("Slot2Enabled", 0);
            //PlayerPrefs.Save ();
        }

        if (PlayerPrefs.HasKey("Slot1Skill"))
        {
            LoadSkill1();
        }
        if (PlayerPrefs.HasKey("Demolish"))
        {
            LoadDemolish();
        }
        if (PlayerPrefs.HasKey("Invisible"))
        {
            LoadInvisible();
        }
        if (PlayerPrefs.HasKey("Magnet"))
        {
            LoadMagnet();
        }
        if (PlayerPrefs.HasKey("Fly"))
        {
            LoadFly();
        }
        if (PlayerPrefs.HasKey("CurrentSkin"))
        {
            currentSkinIndex = PlayerPrefs.GetInt("CurrentSkin");
            skinAvailability = PlayerPrefs.GetInt("SkinAvailability");
            //PlayerPrefs.SetInt ("SkinAvailability", 1);
            //PlayerPrefs.SetInt ("CurrentSkin", 0);
            //PlayerPrefs.Save ();
        }

        if (PlayerPrefs.HasKey("SkenarioUnlocked"))
        {
            LoadUnlockedSkenario();
        }
        if (!PlayerPrefs.HasKey("SkenarioUnlocked"))
        {
            unlockedSkenario = 0;
        }
        if (currentScene.name == "mainmenu")
        {
            if (PlayerPrefs.HasKey("10kscore"))
            {
                LoadScore10();
            }
            if (PlayerPrefs.HasKey("25kscore"))
            {
                LoadScore25();
            }
            if (PlayerPrefs.HasKey("50kscore"))
            {
                LoadScore50();
            }
            if (PlayerPrefs.HasKey("scene1unlocked"))
            {
                LoadUnlockFanta();
            }
            if (PlayerPrefs.HasKey("scene2unlocked"))
            {
                LoadUnlockCandy();
            }
            if (PlayerPrefs.HasKey("scene3unlocked"))
            {
                LoadUnlockRuins();
            }
            if (PlayerPrefs.HasKey("scene4unlocked"))
            {
                LoadUnlockDesert();
            }
            if (PlayerPrefs.HasKey("scene5unlocked"))
            {
                LoadUnlockOcean();
            }
            if (PlayerPrefs.HasKey("allunlocked"))
            {
                LoadUnlockall();
            }
        }



        //--------------------------------------------------------------------------------------------------------------//


        if (!PlayerPrefs.HasKey("JumlahKoin"))
        {
            CoinsOwned = 0;
            if (teksscorecoin != null)
            {
                teksscorecoin.text = CoinsOwned.ToString();
            }
        }
        if (!PlayerPrefs.HasKey("JumlahNyawa"))
        {
            Nyawa = 5;
            if (teksNyawa != null)
            {
                teksNyawa.text = Nyawa.ToString();
            }
        }
        if (!PlayerPrefs.HasKey("JumlahDiamond"))
        {
            jumlahDiamond = 5;
            if (teksdiamond != null)
            {
                teksdiamond.text = jumlahDiamond.ToString();
            }
        }
        if (!PlayerPrefs.HasKey("JumlahHighscore"))
        {
            highscore = 0;
            if (teksHighscoreGameOver != null)
            {
                teksHighscoreGameOver.text = highscore.ToString("F0") + " m";
            }
        }
        if (!PlayerPrefs.HasKey("Slot2Enabled"))
        {
            PlayerPrefs.SetInt("Slot2Enabled", 0);
            PlayerPrefs.Save();
        }
        if (!PlayerPrefs.HasKey("Slot1Skill"))
        {
            PlayerPrefs.SetString("Slot1Skill", "Demolish");
            LoadSkill1();
            PlayerPrefs.Save();
        }
        if (!PlayerPrefs.HasKey("Slot2Skill"))
        {
            PlayerPrefs.SetString("Slot2Skill", "Demolish");
            PlayerPrefs.Save();
        }
        if (!PlayerPrefs.HasKey("Demolish"))
        {
            PlayerPrefs.SetInt("Demolish", 2);
            LoadDemolish();
            PlayerPrefs.Save();
        }
        if (!PlayerPrefs.HasKey("Invisible"))
        {
            PlayerPrefs.SetInt("Invisible", 2);
            LoadInvisible();
            PlayerPrefs.Save();
        }
        if (!PlayerPrefs.HasKey("Magnet"))
        {
            PlayerPrefs.SetInt("Magnet", 2);
            LoadMagnet();
            PlayerPrefs.Save();
        }
        if (!PlayerPrefs.HasKey("Fly"))
        {
            PlayerPrefs.SetInt("Fly", 2);
            LoadFly();
            PlayerPrefs.Save();
        }
        if (!PlayerPrefs.HasKey("CurrentSkin"))
        {
            //Debug.Log ("A");
            PlayerPrefs.SetInt("CurrentSkin", 0);
            PlayerPrefs.SetInt("SkinAvailability", 1);
            currentSkinIndex = PlayerPrefs.GetInt("CurrentSkin");
            skinAvailability = PlayerPrefs.GetInt("SkinAvailability");
            PlayerPrefs.Save();
        }

        if (!PlayerPrefs.HasKey("SkenarioUnlocked"))
        {
            unlockedSkenario = 0;
        }


        //if (!PlayerPrefs.HasKey("10kscore"))
        //{
        //    score10k = 0;
        //}
        //if (!PlayerPrefs.HasKey("25kscore"))
        //{
        //    score25k = 0;
        //}
        //if (!PlayerPrefs.HasKey("50kscore"))
        //{
        //    score50k = 0;
        //}
        //if (!PlayerPrefs.HasKey("scene1unlocked"))
        //{
        //    unlock1 = 0;
        //}
        //if (!PlayerPrefs.HasKey("scene2unlocked"))
        //{
        //    unlock2 = 0;
        //}
        //if (!PlayerPrefs.HasKey("scene3unlocked"))
        //{
        //    unlock3 = 0;
        //}
        //if (!PlayerPrefs.HasKey("scene4unlocked"))
        //{
        //    unlock4 = 0;
        //}
        //if (!PlayerPrefs.HasKey("scene5unlocked"))
        //{
        //    unlock5 = 0;
        //}
        //if (!PlayerPrefs.HasKey("allunlocked"))
        //{
        //    unlockall = 0;
        //}
        Debug.Log(unlockedSkenario);

    }

    public void Savescore(){
        PlayerPrefs.SetFloat("CurrentScore", scoretime);
        PlayerPrefs.Save();
    }

    public void Loadscore(){
        float Currentscoreloaded = PlayerPrefs.GetFloat("CurrentScore");
        scoretime = Currentscoreloaded;
    }

    //public void Resetscore(){
    //    PlayerPrefs.DeleteKey("CurrentScore");
    //}

    public void HitungJarak()
    {
        
        scoretime += Time.deltaTime*3;
        teksjarak.text = scoretime.ToString("F0") + " m";
    }

    //Koin
    public void SaveCoins()
    {
        PlayerPrefs.SetInt("JumlahKoin",CoinsOwned);
        PlayerPrefs.Save();
    }

    public void LoadCoins()
    {
        int koinloaded = PlayerPrefs.GetInt("JumlahKoin");
        CoinsOwned = koinloaded;
      
        //Debug.Log("koin Now = " + CoinsOwned + ", Nyawa Now = "+ Nyawa+ ", Diamond Now = "+jumlahDiamond);
    }

    //Nyawa
    public void SaveNyawa()
    {
        PlayerPrefs.SetInt("JumlahNyawa", Nyawa);
        PlayerPrefs.Save();
    }

    public void LoadNyawa()
    {
        int Nyawaloaded = PlayerPrefs.GetInt("JumlahNyawa");
        Nyawa =  Nyawaloaded;

        //Debug.Log("koin Now = " + CoinsOwned + ", Nyawa Now = " + Nyawa + ", Diamond Now = " + jumlahDiamond);
    }

    //Diamond
    public void SaveDiamond()
    {
        PlayerPrefs.SetInt("JumlahDiamond", jumlahDiamond);
        PlayerPrefs.Save();
    }

    public void LoadDiamond()
    {
        int Diamondloaded = PlayerPrefs.GetInt("JumlahDiamond");
        jumlahDiamond = Diamondloaded;

        //Debug.Log("koin Now = " + CoinsOwned + ", Nyawa Now = " + Nyawa + ", Diamond Now = " + jumlahDiamond);
    }

    //HighScore
    public void SaveHighscore()
    {
        PlayerPrefs.SetFloat("JumlahHighscore", scoretime);
        PlayerPrefs.Save();

        if (highscore >= 10000.0f)
        {
            if (!PlayerPrefs.HasKey("10kscore"))
            {
                SaveScore10();
            }
        }
        if (highscore >= 25000.0f)
        {
            if (!PlayerPrefs.HasKey("25kscore"))
            {
                SaveScore25();
            }
        }
        if (highscore >= 50000.0f)
        {
            if (!PlayerPrefs.HasKey("50kscore"))
            {
                SaveScore50();
            }
        }

    }

    public void LoadHighscore()
    {
        float Highscoreloaded = PlayerPrefs.GetFloat("JumlahHighscore");
        highscore = Highscoreloaded;
    }



	public void LoadSkill1(){
		string skill1Loaded = PlayerPrefs.GetString ("Slot1Skill");
		int skillAmountLoaded = PlayerPrefs.GetInt (skill1Loaded);
        if (teksSkill1 !=null)
		    teksSkill1.text = "x " + skillAmountLoaded.ToString();
	}

	public void LoadSkill2(){
		string skill2Loaded = PlayerPrefs.GetString ("Slot2Skill");
		int skillAmountLoaded = PlayerPrefs.GetInt (skill2Loaded);
        if (teksSkill2 != null)
		    teksSkill2.text = "x " + skillAmountLoaded.ToString();
	}

	public void LoadDemolish(){
		int demolishAmount = PlayerPrefs.GetInt ("Demolish");
        if (teksDemolish != null)
		    teksDemolish.text = "x " + demolishAmount.ToString ();
	}

	public void LoadInvisible(){
		int invisAmount = PlayerPrefs.GetInt ("Invisible");
        if (teksInvis != null)
		    teksInvis.text = "x " + invisAmount.ToString ();
	}

	public void LoadMagnet(){
		int magnetAmount = PlayerPrefs.GetInt ("Magnet");
        if (teksMagnet != null)
		    teksMagnet.text = "x " + magnetAmount.ToString ();
	}

	public void LoadFly(){
		int flyAmount = PlayerPrefs.GetInt ("Fly");
        if (teksFly != null)
		    teksFly.text = "x " + flyAmount.ToString ();
	}

	public void ReloadSkills(){
		LoadSkill1 ();
		if (PlayerPrefs.HasKey ("Slot2Enabled")) {
			if (PlayerPrefs.GetInt ("Slot2Enabled") == 1 && PlayerPrefs.HasKey ("Slot2Skill")) {
				LoadSkill2 ();
			}
		}
		LoadDemolish ();
		LoadInvisible ();
		LoadMagnet ();
		LoadFly ();
	}

    //Skenario
    public void SaveUnlockedSkenario()
    {
        PlayerPrefs.SetInt("SkenarioUnlocked", unlockedSkenario);
        PlayerPrefs.Save();
    }

    public void LoadUnlockedSkenario()
    {
        int SkenarioUnlocked = PlayerPrefs.GetInt("SkenarioUnlocked");
        unlockedSkenario = SkenarioUnlocked;
    }

    public void SaveScore10()
    {
        PlayerPrefs.SetInt("10kscore", 1);
        PlayerPrefs.Save();
        CoinsOwned = CoinsOwned + 10000;
        SaveCoins();
    }
    public void SaveScore25()
    {
        PlayerPrefs.SetInt("25kscore", 1);
        PlayerPrefs.Save();
        CoinsOwned = CoinsOwned + 15000;
        SaveCoins();
    }
    public void SaveScore50()
    {
        PlayerPrefs.SetInt("50kscore", 1);
        PlayerPrefs.Save();
        CoinsOwned = CoinsOwned + 25000;
        SaveCoins();
    }

    public void SaveUnlockFanta()
    {
        PlayerPrefs.SetInt("scene1unlocked", 1);
        PlayerPrefs.Save();
        CoinsOwned = CoinsOwned + 500;
        SaveCoins();
    }
    public void SaveUnlockCandy()
    {
        PlayerPrefs.SetInt("scene2unlocked", 1);
        PlayerPrefs.Save();
        CoinsOwned = CoinsOwned + 1000;
        SaveCoins();
    }
    public void SaveUnlockRuins()
    {
        PlayerPrefs.SetInt("scene3unlocked", 1);
        PlayerPrefs.Save();
        CoinsOwned = CoinsOwned + 1500;
        SaveCoins();
    }
    public void SaveUnlockDesert()
    {
        PlayerPrefs.SetInt("scene4unlocked", 1);
        PlayerPrefs.Save();
        CoinsOwned = CoinsOwned + 2000;
        SaveCoins();
    }
    public void SaveUnlockOcean()
    {
        PlayerPrefs.SetInt("scene5unlocked", 1);
        PlayerPrefs.Save();
        CoinsOwned = CoinsOwned + 2500;
        SaveCoins();
    }

    public void SaveUnlockall()
    {
        PlayerPrefs.SetInt("allunlocked", 1);
        PlayerPrefs.Save();
    }



    public void LoadScore10()
    {
        //int score10 = PlayerPrefs.GetInt("10kscore");
        //score10k = score10;
        //if (score10k == 1)
            tick10k.SetActive(true);
    }
    public void LoadScore25()
    {
        //int score25 = PlayerPrefs.GetInt("25kscore");
        //score25k = score25;
        //if (score25k == 1)
            tick25k.SetActive(true);
    }
    public void LoadScore50()
    {
        //int score50 = PlayerPrefs.GetInt("50kscore");
        //score50k = score50;
        //if (score50k == 1)
            tick50k.SetActive(true);
    }

    public void LoadUnlockFanta()
    {
        //int unlocksc1 = PlayerPrefs.GetInt("scene1unlocked");
        //unlock1 = unlocksc1;
        //if (unlock1 == 1)
            tickfanta.SetActive(true);
    }
    public void LoadUnlockCandy()
    {
        //int unlocksc2 = PlayerPrefs.GetInt("scene2unlocked");
        //unlock2 = unlocksc2;
        tickcandy.SetActive(true);
    }
    public void LoadUnlockRuins()
    {
        //int unlocksc3 = PlayerPrefs.GetInt("scene3unlocked");
        //unlock3 = unlocksc3;
        tickruins.SetActive(true);
    }
    public void LoadUnlockDesert()
    {
        //int unlocksc4 = PlayerPrefs.GetInt("scene4unlocked");
        //unlock4 = unlocksc4;
        tickdeser.SetActive(true);
    }
    public void LoadUnlockOcean()
    {
        //int unlocksc5 = PlayerPrefs.GetInt("scene5unlocked");
        //unlock5 = unlocksc5;
        tickocean.SetActive(true);
    }

    public void LoadUnlockall()
    {
        //int allunlock = PlayerPrefs.GetInt("allunlocked");
        //unlockall = allunlock;
        tickall.SetActive(true);
    }

    public void achievementscenario()
    {
        if (unlockedSkenario == 1){
            if (!PlayerPrefs.HasKey("scene1unlocked"))
                SaveUnlockFanta();
        }
        if (unlockedSkenario == 2)
        {
            if (!PlayerPrefs.HasKey("scene2unlocked"))
              SaveUnlockCandy();
        }
        if (unlockedSkenario == 3)
        {
            if (!PlayerPrefs.HasKey("scene3unlocked"))
                SaveUnlockRuins();
        }
        if (unlockedSkenario == 4)
        {
            if (!PlayerPrefs.HasKey("scene4unlocked"))
                SaveUnlockDesert();
        }
        if (unlockedSkenario == 5)
        {
            if (!PlayerPrefs.HasKey("scene5unlocked"))
               SaveUnlockOcean();
        }
    }

}
