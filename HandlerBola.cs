﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HandlerBola : MonoBehaviour {

    private GameVariables gamevarscript;
    private GameEventManager eventscr;
    private HandlerUlti ultiscript;
    private GameObject spawner;


    public float maxJumpHeight = 3.5f;
	public float speed =  1;

    void Start(){
        
        gamevarscript = GameObject.FindGameObjectWithTag("Spawner").GetComponent(typeof(GameVariables)) as GameVariables;
        eventscr = GameObject.FindGameObjectWithTag("Spawner").GetComponent(typeof(GameEventManager)) as GameEventManager;
        ultiscript = gameObject.GetComponent(typeof(HandlerUlti)) as HandlerUlti;
    }

    private void Update()
    {

        //khusus untuk hitungjarak
        if (!eventscr.gameOver)
        {
            if (!eventscr.pause)
            {
                if (speed <= 30){
                    speed += Time.deltaTime * 0.05f;
                }

                if(!eventscr.tekscountdowngame.gameObject.activeInHierarchy)
                {
                    gamevarscript.HitungJarak();
                  
                }

            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Koin")
        {
            Destroy(col.gameObject);
            eventscr.GetCoin();
        }
        if (col.gameObject.tag == "Obstacle")
        {
			if (ultiscript.ulti != HandlerUlti.Ultimate.HANTAM && ultiscript.ulti != HandlerUlti.Ultimate.TEMBUS)
            {
                eventscr.OnGameOver();
                this.gameObject.GetComponent<Collider>().enabled = false;
            }
        }

        if (col.gameObject.tag == "Bouncer")
        {
            StartCoroutine("Lompat");
        }
    }


    public IEnumerator Lompat()
    {
        while (transform.position.y < maxJumpHeight)
        {
            transform.position += Vector3.up / 2f;
            yield return new WaitForSeconds(Time.smoothDeltaTime / 100f);
        }



        while (transform.localPosition.y > 0.3859205f)
        {
            transform.position += Vector3.down / 5f;
            yield return new WaitForSeconds(Time.smoothDeltaTime / 100f);
        }

        yield return new WaitForEndOfFrame();
    }
}
