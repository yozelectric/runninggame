﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandlerUlti : MonoBehaviour {
    
    //Variabel
    public enum Ultimate { TEMBUS, HANTAM, MAGNET, TERBANG, GAAKTIF };
    public Ultimate ulti;
    public float ultDur;
    public float elevasi;
    public float defY;
	public Transform psTerbang;
	public Transform psTembus;
	public Transform psHantam;

	private GameObject[] coins;
	private float currentTime;
	private float searchTime;

	// Use this for initialization
	void Awake () {
        ulti = Ultimate.GAAKTIF;
		currentTime = 0f;
		searchTime = 0.5f;
	}
	
	// Update is called once per frame
	void Update () {
        switch (ulti)
        {
        case Ultimate.TEMBUS:
            StartCoroutine("Tembus");
            break;
        case Ultimate.HANTAM:
            StartCoroutine("Hantam");
            break;
        case Ultimate.MAGNET:
            StartCoroutine("Magnet");
            break;
        case Ultimate.TERBANG:
            StartCoroutine("Terbang");
            break;
		default:
			ulti = Ultimate.GAAKTIF;
			break;
        }
//		Debug.Log (ulti);
	}
    //Fungsi

	public void UpdateCoins(){
		coins = GameObject.FindGameObjectsWithTag ("Koin");
	}

    // Set Ulti
    public void SetUltimateActive(Ultimate ultimate)
    {
        ulti = ultimate;
    }
    
    //Ulti invis
    public IEnumerator Tembus()
    {
		psTembus.gameObject.SetActive (true);
        yield return new WaitForSeconds(ultDur);
        ulti = Ultimate.GAAKTIF;
		psTembus.gameObject.SetActive (false);
        yield break;
    }

    //Ulti invul
    public IEnumerator Hantam()
    {
		psHantam.gameObject.SetActive (true);
        yield return new WaitForSeconds(ultDur);
        ulti = Ultimate.GAAKTIF;
		psHantam.gameObject.SetActive (false);
        yield break;
    }

    //Ulti magnet
    public IEnumerator Magnet()
    {
		Collider[] colls;
		colls = Physics.OverlapSphere (transform.position, 5f);
		foreach (Collider c in colls) {
			if (c != null && c.gameObject.tag == "Koin") {
				if (Vector3.Distance (transform.position, c.transform.position) > 0.05f) {
					c.transform.position = Vector3.Lerp (c.transform.position, transform.position, Time.deltaTime * 20f);
				} else {
					c.transform.position = transform.position;
				}
			}
		}
		yield return new WaitForSeconds (ultDur);
        ulti = Ultimate.GAAKTIF;
        yield break;
    }

    public IEnumerator Terbang()
    {
        while (transform.position.y <= elevasi)
        {
            transform.position += Vector3.up / 100f;
            yield return new WaitForSeconds(Time.deltaTime / 100f);
        }

		psTerbang.gameObject.SetActive (true);
        yield return new WaitForSeconds(ultDur - 1f);
		psTerbang.gameObject.SetActive (false);

        while (transform.localPosition.y > defY)
        {
            transform.position += Vector3.down / 100f;
            yield return new WaitForSeconds(Time.deltaTime / 100f);
        }

        ulti = Ultimate.GAAKTIF;
        yield break;
    }
}
