﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class IAPManager : MonoBehaviour, IStoreListener
{
	public static IAPManager Instance{ set; get; }

	public Transform[] diamondTexts;
	public Transform[] coinTexts;

	private static IStoreController m_StoreController;          // The Unity Purchasing system.
	private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

	public static string productDiamond10 =  "diamond10";
	public static string productDiamond32 =  "diamond32";
	public static string productDiamond68 =  "diamond68";
	public static string productDiamond120 =  "diamond120";
	public static string productDiamond250 =  "diamond250";
	public static string productDiamond650 =  "diamond650";

	public static string productDisableAds =  "noads";
	public static string productSkill2 =  "skill2";

	private static string kProductNameAppleSubscription =  "com.unity3d.subscription.new";

	private static string kProductNameGooglePlaySubscription =  "com.unity3d.subscription.original"; 

	void Start()
	{
		if (m_StoreController == null)
		{
			InitializePurchasing();
		}

		UpdateDiamonds ();
		UpdateCoins ();
	}

	public void InitializePurchasing() 
	{
		if (IsInitialized())
		{
			return;
		}

		var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

		builder.AddProduct(productDiamond10, ProductType.Consumable);
		builder.AddProduct(productDiamond32, ProductType.Consumable);
		builder.AddProduct(productDiamond68, ProductType.Consumable);
		builder.AddProduct(productDiamond120, ProductType.Consumable);
		builder.AddProduct(productDiamond250, ProductType.Consumable);
		builder.AddProduct(productDiamond650, ProductType.Consumable);
		builder.AddProduct(productDisableAds, ProductType.Consumable);
		builder.AddProduct(productSkill2, ProductType.Consumable);

		UnityPurchasing.Initialize(this, builder);
	}


	private bool IsInitialized()
	{
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}

	public void BuyDiamond10(){
		BuyProductID (productDiamond10);
	}

	public void BuyDiamond32(){
		BuyProductID (productDiamond32);
	}

	public void BuyDiamond68(){
		BuyProductID (productDiamond68);
	}

	public void BuyDiamond120(){
		BuyProductID (productDiamond120);
	}

	public void BuyDiamond250(){
		BuyProductID (productDiamond250);
	}

	public void BuyDiamond650(){
		BuyProductID (productDiamond650);
	}

	public void BuyDisableAds(){
		BuyProductID (productDisableAds);
	}

	public void BuySkill2(){
		BuyProductID (productSkill2);
	}

	void BuyProductID(string productId)
	{
		if (IsInitialized())
		{
			Product product = m_StoreController.products.WithID(productId);

			if (product != null && product.availableToPurchase)
			{
				Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
				m_StoreController.InitiatePurchase(product);
			}
			else
			{
				Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
			}
		}
		else
		{
			Debug.Log("BuyProductID FAIL. Not initialized.");
		}
	}
		
	public void RestorePurchases()
	{
		if (!IsInitialized())
		{
			Debug.Log("RestorePurchases FAIL. Not initialized.");
			return;
		}

		if (Application.platform == RuntimePlatform.IPhonePlayer || 
			Application.platform == RuntimePlatform.OSXPlayer)
		{
			Debug.Log("RestorePurchases started ...");

			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
			apple.RestoreTransactions((result) => {
				Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
			});
		}
		else
		{
			Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
		}
	}

	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		Debug.Log("OnInitialized: PASS");

		m_StoreController = controller;
		m_StoreExtensionProvider = extensions;
	}


	public void OnInitializeFailed(InitializationFailureReason error)
	{
		Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
	}


	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
	{
		int diamondsOwned;

		if (String.Equals(args.purchasedProduct.definition.id, productDiamond10, StringComparison.Ordinal))
		{
			Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			if(PlayerPrefs.HasKey("JumlahDiamond")){
				diamondsOwned = PlayerPrefs.GetInt ("JumlahDiamond") + 10;
				Debug.Log (diamondsOwned);
				PlayerPrefs.SetInt ("JumlahDiamond", diamondsOwned);
				PlayerPrefs.Save ();
				UpdateDiamonds ();
			}
		}
		else if (String.Equals(args.purchasedProduct.definition.id, productDiamond32, StringComparison.Ordinal))
		{
			Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			if(PlayerPrefs.HasKey("JumlahDiamond")){
				diamondsOwned = PlayerPrefs.GetInt ("JumlahDiamond") + 32;
				Debug.Log (diamondsOwned);
				PlayerPrefs.SetInt ("JumlahDiamond", diamondsOwned);
				PlayerPrefs.Save ();
				UpdateDiamonds ();
			}
		}
		else if (String.Equals(args.purchasedProduct.definition.id, productDiamond68, StringComparison.Ordinal))
		{
			Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			if(PlayerPrefs.HasKey("JumlahDiamond")){
				diamondsOwned = PlayerPrefs.GetInt ("JumlahDiamond") + 68;
				Debug.Log (diamondsOwned);
				PlayerPrefs.SetInt ("JumlahDiamond", diamondsOwned);
				PlayerPrefs.Save ();
				UpdateDiamonds ();
			}
		}
		else if (String.Equals(args.purchasedProduct.definition.id, productDiamond120, StringComparison.Ordinal))
		{
			Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			if(PlayerPrefs.HasKey("JumlahDiamond")){
				diamondsOwned = PlayerPrefs.GetInt ("JumlahDiamond") + 120;
				Debug.Log (diamondsOwned);
				PlayerPrefs.SetInt ("JumlahDiamond", diamondsOwned);
				PlayerPrefs.Save ();
				UpdateDiamonds ();
			}
		}
		else if (String.Equals(args.purchasedProduct.definition.id, productDiamond250, StringComparison.Ordinal))
		{
			Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			if(PlayerPrefs.HasKey("JumlahDiamond")){
				diamondsOwned = PlayerPrefs.GetInt ("JumlahDiamond") + 250;
				Debug.Log (diamondsOwned);
				PlayerPrefs.SetInt ("JumlahDiamond", diamondsOwned);
				PlayerPrefs.Save ();
				UpdateDiamonds ();
			}
		}
		else if (String.Equals(args.purchasedProduct.definition.id, productDiamond650, StringComparison.Ordinal))
		{
			Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			if(PlayerPrefs.HasKey("JumlahDiamond")){
				diamondsOwned = PlayerPrefs.GetInt ("JumlahDiamond") + 650;
				Debug.Log (diamondsOwned);
				PlayerPrefs.SetInt ("JumlahDiamond", diamondsOwned);
				PlayerPrefs.Save ();
				UpdateDiamonds ();
			}
		}
		else 
		{
			Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
		}
		return PurchaseProcessingResult.Complete;
	}


	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
	}

	private void UpdateDiamonds(){
		foreach (Transform diamondText in diamondTexts) {
			diamondText.GetComponent<Text> ().text = PlayerPrefs.GetInt ("JumlahDiamond").ToString ();
		}
	}

	private void UpdateCoins(){
		foreach (Transform coinText in coinTexts) {
			coinText.GetComponent<Text> ().text = PlayerPrefs.GetInt ("JumlahKoin").ToString ();
		}
	}

	public void UpdateCurrency(){
		UpdateCoins ();
		UpdateDiamonds ();	
	}
}