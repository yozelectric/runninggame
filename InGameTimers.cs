﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameTimers : MonoBehaviour {

    private GameVariables gamevarscript;
    private AdsManager adsmanager;
    private GameEventManager eventscr;
    public float hitungmundur = 3.5f;

    private void Awake()
    {
        adsmanager = transform.GetComponent(typeof(AdsManager)) as AdsManager;
        eventscr = transform.GetComponent(typeof(GameEventManager)) as GameEventManager;
        gamevarscript = transform.GetComponent(typeof(GameVariables)) as GameVariables;
    }
    // Use this for initialization
    void Start () {
        

        eventscr.ingame = true;
        eventscr.mainmenu = false;
	}


    void Update()
    {
        //khusus untuk timer mulai game
        if (eventscr.tekscountdowngame.gameObject.activeInHierarchy)
        {
            if (!eventscr.pause){
                hitungmundur -= Time.deltaTime;
            }

            if (hitungmundur < 0.6f)
            {
                eventscr.tekscountdowngame.text = "GO!";
            }
            else
                eventscr.tekscountdowngame.text = hitungmundur.ToString("F0");

            if (hitungmundur <= -0.1f)
            {
                eventscr.tekscountdowngame.gameObject.SetActive(false);
                eventscr.pausebutton.gameObject.SetActive(true);
                hitungmundur = 3.5f;
            }
        }
    }
}
