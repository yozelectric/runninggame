﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuPlanarHandler : MonoBehaviour {

//	private GameEventManager eventscr;
    private MainMenuScript scriptspawner;

	// Use this for initialization
	void Start () {
		//eventscr = GameObject.FindGameObjectWithTag("Spawner").GetComponent(typeof(GameEventManager)) as GameEventManager;
        scriptspawner= GameObject.FindGameObjectWithTag ("Spawner").GetComponent (typeof(MainMenuScript))as MainMenuScript;


	}
	// Update is called once per frame
	void Update () {
        
		transform.Translate (0, 0.15f, 0);
		if (transform.localPosition.z < -30) {
			scriptspawner.SpawnPlanar ();
			Destroy (this.gameObject);
		}
	}
}
