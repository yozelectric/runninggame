﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuScript : MonoBehaviour {

    public Transform prefabLoop;

    private AdsManager adsmanager;
    private GameVariables gamevarscript;
    private GameEventManager eventscr;
    //  private AdsManager adsmanagerscript;

    private void Awake()
    {
        adsmanager = transform.GetComponent(typeof(AdsManager)) as AdsManager;
        eventscr = transform.GetComponent(typeof(GameEventManager)) as GameEventManager;
        gamevarscript = transform.GetComponent(typeof(GameVariables)) as GameVariables;

    }

    // Use this for initialization
    void Start () {
        if (adsmanager.bannerView == null )
            adsmanager.RequestBanner();
        if (gamevarscript.Nyawa == 0)
        {
            adsmanager.RequestRewardedVideo();
        }

        //gamevarscript.tekscoinShop.text = gamevarscript.CoinsOwned.ToString();
        //gamevarscript.teksDiamondShop.text = "0";

        eventscr.ingame = false;
        eventscr.mainmenu = true;
	}

    public void SpawnPlanar()
    {
        Instantiate(prefabLoop);
    }
}
