﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillHandler : MonoBehaviour {

	public Transform mainBall;
	public HandlerUlti.Ultimate skillS1;
	public HandlerUlti.Ultimate skillS2;

	[Header("Skill 1")]
	public Transform skill1Button;
	public Image skill1CDImage;
	public float skill1CD;
	public Text skill1AmountText;

	[Header("Skill 2")]
	public Transform skill2Button;
	public Image skill2CDImage;
	public float skill2CD;
	public Text skill2AmountText;

	private float skill1CDTimer;
	private float skill2CDTimer;
	private string skill1;
	private string skill2;

	void Awake(){
		skill1CDTimer = 0;
		skill2CDTimer = 0;

		if (PlayerPrefs.HasKey ("Slot1Skill")) {
			skill1 = PlayerPrefs.GetString ("Slot1Skill");
		}

		if (PlayerPrefs.HasKey ("Slot2Enabled")) {
			if (PlayerPrefs.GetInt ("Slot2Enabled") == 1) {
				skill2CDImage.gameObject.SetActive (true);
				skill2 = PlayerPrefs.GetString ("Slot2Skill");
			} else {
				skill2Button.GetComponent<Button> ().interactable = false;
			}

			switch (skill1) {
			case "Demolish":
				skillS1 = HandlerUlti.Ultimate.HANTAM;
				break;
			case "Invisible":
				skillS1 = HandlerUlti.Ultimate.TEMBUS;
				break;
			case "Magnet":
				skillS1 = HandlerUlti.Ultimate.MAGNET;
				break;
			case "Fly":
				skillS1 = HandlerUlti.Ultimate.TERBANG;
				break;
			}

			switch (skill2) {
			case "Demolish":
				skillS2 = HandlerUlti.Ultimate.HANTAM;
				break;
			case "Invisible":
				skillS2 = HandlerUlti.Ultimate.TEMBUS;
				break;
			case "Magnet":
				skillS2 = HandlerUlti.Ultimate.MAGNET;
				break;
			case "Fly":
				skillS2 = HandlerUlti.Ultimate.TERBANG;
				break;
			}
		}
	}

	public void ActivateSkill(int slot){
		if (slot == 1 && PlayerPrefs.GetInt(skill1) > 0) {
			mainBall.GetComponent<HandlerUlti> ().ulti = skillS1;
			skill1CDTimer = skill1CD;
		} else if (slot == 2 && PlayerPrefs.GetInt(skill2) > 0) {
			mainBall.GetComponent<HandlerUlti> ().ulti = skillS2;
			skill2CDTimer = skill2CD;
		}

		//Debug.Log (skill);
	}

	void Update(){
		if (skill1CDTimer > 0) {
			skill1CDTimer -= Time.deltaTime;
			skill1CDImage.fillAmount = skill1CDTimer / skill1CD;
			skill2CDImage.fillAmount = skill1CDTimer / skill1CD;
			skill1Button.GetComponent<Button> ().interactable = false;
			skill2Button.GetComponent<Button> ().interactable = false;
		}

		if (skill1CDTimer < 0) {
			skill1CDTimer = 0;
			skill1Button.GetComponent<Button> ().interactable = true;
			skill2Button.GetComponent<Button> ().interactable = true;
			//mainBall.GetComponent<HandlerUlti> ().ulti = HandlerUlti.Ultimate.GAAKTIF;
			switch (skill1) {
			case "Demolish":
				mainBall.GetComponent<HandlerUlti> ().StopCoroutine ("Hantam");
				mainBall.GetComponent<HandlerUlti> ().psHantam.gameObject.SetActive (false);
				break;
			case "Invisible":
				mainBall.GetComponent<HandlerUlti> ().StopCoroutine ("Tembus");
				mainBall.GetComponent<HandlerUlti> ().psTembus.gameObject.SetActive (false);
				break;
			case "Magnet":
				mainBall.GetComponent<HandlerUlti> ().StopCoroutine ("Magnet");
				break;
			case "Fly":				
				if (mainBall.transform.localPosition.y <= mainBall.GetComponent<HandlerUlti> ().defY) {
					mainBall.GetComponent<HandlerUlti> ().StopCoroutine ("Terbang");
				}
				break;
			}
			mainBall.GetComponent<HandlerUlti> ().ulti = HandlerUlti.Ultimate.GAAKTIF;
		}

		if (skill2CDTimer > 0) {
			skill2CDTimer -= Time.deltaTime;
			skill2CDImage.fillAmount = skill2CDTimer / skill2CD;
			skill1CDImage.fillAmount = skill2CDTimer / skill2CD;
			skill2Button.GetComponent<Button> ().interactable = false;
			skill1Button.GetComponent<Button> ().interactable = false;
		}

		if (skill2CDTimer < 0) {
			skill2CDTimer = 0;
			skill2Button.GetComponent<Button> ().interactable = true;
			skill1Button.GetComponent<Button> ().interactable = true;
			//mainBall.GetComponent<HandlerUlti> ().ulti = HandlerUlti.Ultimate.GAAKTIF;
			switch (skill2) {
			case "Demolish":
				mainBall.GetComponent<HandlerUlti> ().StopCoroutine ("Hantam");
				mainBall.GetComponent<HandlerUlti> ().psHantam.gameObject.SetActive (false);
				break;
			case "Invisible":
				mainBall.GetComponent<HandlerUlti> ().StopCoroutine ("Tembus");
				mainBall.GetComponent<HandlerUlti> ().psTembus.gameObject.SetActive (false);
				break;
			case "Magnet":
				mainBall.GetComponent<HandlerUlti> ().StopCoroutine ("Magnet");
				break;
			case "Fly":				
				if (mainBall.transform.localPosition.y <= mainBall.GetComponent<HandlerUlti> ().defY) {
					mainBall.GetComponent<HandlerUlti> ().StopCoroutine ("Terbang");
				}
				break;
			}
			mainBall.GetComponent<HandlerUlti> ().ulti = HandlerUlti.Ultimate.GAAKTIF;
		}
	}
}
