﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillLoader : MonoBehaviour {

	[Header("Skill 1")]
	public Image skill1Image;
	public Text skill1Text;

	[Header("Skill 2")]
	public Image skill2Image;
	public Text skill2Text;

	[Header("Required Sprites")]
	public Sprite demolishSprite;
	public Sprite invisSprite;
	public Sprite magnetSprite;
	public Sprite flySprite;
	public Sprite reviveSprite;

	[Header("Ball")]
	public Transform skillHandlerObj;

	private string skill1;
	private string skill2;
	private int demolishAmt;
	private int invisAmt;
	private int magnetAmt;
	private int flyAmt;
	private int reviveAmt;
	private SkillHandler skillScript;

	void Awake(){
		skillScript = skillHandlerObj.GetComponent<SkillHandler> ();

		if (PlayerPrefs.HasKey ("Slot1Skill"))
			skill1 = PlayerPrefs.GetString ("Slot1Skill");

		if (PlayerPrefs.HasKey ("Demolish"))
			demolishAmt = PlayerPrefs.GetInt ("Demolish");

		if (PlayerPrefs.HasKey ("Invisible"))
			invisAmt = PlayerPrefs.GetInt ("Invisible");

		if (PlayerPrefs.HasKey ("Magnet"))
			magnetAmt = PlayerPrefs.GetInt ("Magnet");

		if (PlayerPrefs.HasKey ("Fly"))
			flyAmt = PlayerPrefs.GetInt ("Fly");

		if (PlayerPrefs.HasKey ("Revive"))
			reviveAmt = PlayerPrefs.GetInt ("Revive");
		
		if (PlayerPrefs.HasKey ("Slot2Enabled")) {			
			if (PlayerPrefs.GetInt ("Slot2Enabled") == 1 && PlayerPrefs.HasKey ("Slot2Skill")) {
				skill2 = PlayerPrefs.GetString ("Slot2Skill");
				skill2Text.gameObject.SetActive (true);
				switch (skill2) {
				case "Demolish":
					skill2Image.sprite = demolishSprite;
					skill2Text.text = demolishAmt.ToString ();
					break;
				case "Invisible":
					skill2Image.sprite = invisSprite;
					skill2Text.text = invisAmt.ToString ();
					break;
				case "Magnet":
					skill2Image.sprite = magnetSprite;
					skill2Text.text = magnetAmt.ToString ();
					break;
				case "Fly":
					skill2Image.sprite = flySprite;
					skill2Text.text = flyAmt.ToString ();
					break;
				case "Revive":
					skill2Image.sprite = reviveSprite;
					skill2Text.text = reviveAmt.ToString ();
					break;
				}
			}
		}

		switch (skill1) {
		case "Demolish":
			skill1Image.sprite = demolishSprite;
			skill1Text.text = demolishAmt.ToString ();
			break;
		case "Invisible":
			skill1Image.sprite = invisSprite;
			skill1Text.text = invisAmt.ToString ();
			break;
		case "Magnet":
			skill1Image.sprite = magnetSprite;
			skill1Text.text = magnetAmt.ToString ();
			break;
		case "Fly":
			skill1Image.sprite = flySprite;
			skill1Text.text = flyAmt.ToString ();
			break;
		case "Revive":
			skill1Image.sprite = reviveSprite;
			skill1Text.text = reviveAmt.ToString ();
			break;
		}
	}

	public void ActivateSkill2(){
		if (PlayerPrefs.HasKey ("Slot2Enabled")) {
			if (PlayerPrefs.GetInt ("Slot2Enabled") == 1 && PlayerPrefs.HasKey ("Slot2Skill")) {
				skillScript.ActivateSkill (2);
			}
		}
	}
}
