﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillShopManager : MonoBehaviour {

	[Header("Sprites")]
	public Sprite spriteDemolish;
	public Sprite spriteInvis;
	public Sprite spriteMagnet;
	public Sprite spriteFly;
	public Sprite spriteRevive;

	[Header("Equipped Skills")]
	public Text teksSkill1;
	public Text teksSkill2;
	public Text teksDemolish;
	public Text teksInvis;
	public Text teksMagnet;
	public Text teksFly;
	public Text teksRevive;

	[Header("Target Slot")]
	public Image skill1Image;
	public Image skill2Image;

	[Header("Purchase Confirmation")]
	public Image panel;
	public Image purchaseImage;
	public Text purchaseText;

	[Header("Slot 2 Confirmation")]
	public Image s2Panel;
	public Image s2PurchaseImage;
	public Text s2PurchaseText;
	public Text s2ConfirmText;

	[Header("Not Enough Currency")]
	public Image nePanel;
	public Text neText;

	private string skillPurchase;
	private string purchaseMethod;

	private int selectedSlot;
	private int slot2Enabled;

	void Awake(){
		slot2Enabled = PlayerPrefs.GetInt ("Slot2Enabled");
		if (slot2Enabled == 1) {
			//skill2Image.gameObject.SetActive (true);
			teksSkill2.gameObject.SetActive (true);
			//ShowEquippedSkill ("Slot2Skill", skill2Image);
			string loadedSkill = PlayerPrefs.GetString ("Slot2Skill");
			switch (loadedSkill) {
			case "Demolish":
				skill2Image.sprite = spriteDemolish;
				break;

			case "Invisible":
				skill2Image.sprite = spriteInvis;
				break;

			case "Magnet":
				skill2Image.sprite = spriteMagnet;
				break;

			case "Fly":
				skill2Image.sprite = spriteFly;
				break;

			case "Revive":
				skill2Image.sprite = spriteRevive;
				break;
			}
		}
		ShowEquippedSkill ("Slot1Skill", skill1Image);
		selectedSlot = 1;
	}

	public void SetSelectedSlot(int val){
		if (val == 1) {
			selectedSlot = val;
			UnequipSkills ();
			ShowEquippedSkill ("Slot1Skill", skill1Image);
		} else if (val == 2 && PlayerPrefs.GetInt("Slot2Enabled") == 1) {
			selectedSlot = val;
			UnequipSkills ();
			ShowEquippedSkill ("Slot2Skill", skill2Image);
		} else {
			Debug.Log ("Slot 2 is locked");
			s2PurchaseImage.gameObject.SetActive (false);
			s2PurchaseText.text = "10 diamonds";
			s2ConfirmText.text = "UNLOCK 2ND SKILL SLOT?";
			s2Panel.gameObject.SetActive (true);
		}
		//Debug.Log ("Selected slot = " + selectedSlot);
	}

	public int GetSelectedSlot(){
		return selectedSlot;
	}

	public void ShowEquippedSkill(string val, Image img){
		string loadedSkill = PlayerPrefs.GetString (val);
		switch (loadedSkill) {
		case "Demolish":
			img.sprite = spriteDemolish;
			teksDemolish.gameObject.SetActive (true);
			break;

		case "Invisible":
			img.sprite = spriteInvis;
			teksInvis.gameObject.SetActive (true);
			break;

		case "Magnet":
			img.sprite = spriteMagnet;
			teksMagnet.gameObject.SetActive (true);
			break;

		case "Fly":
			img.sprite = spriteFly;
			teksFly.gameObject.SetActive (true);
			break;

		case "Revive":
			img.sprite = spriteRevive;
			teksRevive.gameObject.SetActive (true);
			break;
		}
	}

	public void EquipSkill(string val){
		switch (val) {
		case "Demolish":
			UnequipSkills ();
			if (selectedSlot == 1) {
				skill1Image.sprite = spriteDemolish;
			} else {
				skill2Image.sprite = spriteDemolish;
			}
			teksDemolish.gameObject.SetActive (true);
			break;

		case "Invisible":
			UnequipSkills ();
			if (selectedSlot == 1) {
				skill1Image.sprite = spriteInvis;
			} else {
				skill2Image.sprite = spriteInvis;
			}
			teksInvis.gameObject.SetActive (true);
			break;

		case "Magnet":
			UnequipSkills ();
			if (selectedSlot == 1) {
				skill1Image.sprite = spriteMagnet;
			} else {
				skill2Image.sprite = spriteMagnet;
			}
			teksMagnet.gameObject.SetActive (true);
			break;

		case "Fly":
			UnequipSkills ();
			if (selectedSlot == 1) {
				skill1Image.sprite = spriteFly;
			} else {
				skill2Image.sprite = spriteFly;
			}
			teksFly.gameObject.SetActive (true);
			break;

		case "Revive":
			UnequipSkills ();
			if (selectedSlot == 1) {
				skill1Image.sprite = spriteRevive;
			} else {
				skill2Image.sprite = spriteRevive;
			}
			teksRevive.gameObject.SetActive (true);
			break;
		}

		if (selectedSlot == 1) {
			PlayerPrefs.SetString ("Slot1Skill", val);
			teksSkill1.text = "x " + PlayerPrefs.GetInt (val);
		} else {
			PlayerPrefs.SetString ("Slot2Skill", val);
			teksSkill2.text = "x " + PlayerPrefs.GetInt (val);
		}

		PlayerPrefs.Save ();
	}

	public void UnequipSkills(){
		teksDemolish.gameObject.SetActive (false);
		teksInvis.gameObject.SetActive (false);
		teksMagnet.gameObject.SetActive (false);
		teksFly.gameObject.SetActive (false);
	}

	public void SetMethod(string currency){
		if (currency == "coin") {
			int currentCoin = PlayerPrefs.GetInt ("JumlahKoin");
			if (currentCoin < 10000) {
				Debug.Log ("Koin tidak cukup");
				neText.text = "NOT ENOUGH COINS.";
				nePanel.gameObject.SetActive (true);
				return;
			}
			purchaseText.text = "10000 coins";
		} else {
			int currentDiamond = PlayerPrefs.GetInt ("JumlahDiamond");
			if (currentDiamond < 10) {
				Debug.Log ("Diamond tidak cukup");
				neText.text = "NOT ENOUGH DIAMOND.";
				nePanel.gameObject.SetActive (true);
				return;
			}
			purchaseText.text = "10 diamonds";
		}

		purchaseMethod = currency;

		panel.gameObject.SetActive (true);
	}

	public void BuySkill(string val){
		switch (val) {
		case "Demolish":
			purchaseImage.sprite = spriteDemolish;
			break;

		case "Invisible":
			purchaseImage.sprite = spriteInvis;
			break;

		case "Magnet":
			purchaseImage.sprite = spriteMagnet;
			break;

		case "Fly":
			purchaseImage.sprite = spriteFly;
			break;

		case "Revive":
			purchaseImage.sprite = spriteRevive;
			break;
		}

		skillPurchase = val;
	}

	public void ContinuePurchase(){
		string targetKey;
		int curPrice;
		int curAmount;
		int skillAmountLoaded;

		if (purchaseMethod == "coin") {
			targetKey = "JumlahKoin";
			curPrice = 10000;
		} else {
			targetKey = "JumlahDiamond";
			curPrice = 10;
		}

		curAmount = PlayerPrefs.GetInt (targetKey);
		PlayerPrefs.SetInt (targetKey, (curAmount - curPrice));

		skillAmountLoaded = PlayerPrefs.GetInt (skillPurchase);
		PlayerPrefs.SetInt(skillPurchase, (skillAmountLoaded + 5));
		PlayerPrefs.Save ();

		neText.text = "PURCHASE SUCCESSFUL.";

		panel.gameObject.SetActive (false);
		nePanel.gameObject.SetActive (true);
	}

	public void BuySlot2(){
		int curDiamonds;

		curDiamonds = PlayerPrefs.GetInt ("JumlahDiamond");
		if (curDiamonds >= 10) {
			PlayerPrefs.SetInt ("JumlahDiamond", curDiamonds - 10);
			PlayerPrefs.SetInt ("Slot2Enabled", 1);
			neText.text = "PURCHASE SUCCESSFUL";
			teksSkill2.gameObject.SetActive (true);

			string loadedSkill = PlayerPrefs.GetString ("Slot2Skill");
			switch (loadedSkill) {
			case "Demolish":
				skill2Image.sprite = spriteDemolish;
				break;

			case "Invisible":
				skill2Image.sprite = spriteInvis;
				break;

			case "Magnet":
				skill2Image.sprite = spriteMagnet;
				break;

			case "Fly":
				skill2Image.sprite = spriteFly;
				break;

			case "Revive":
				skill2Image.sprite = spriteRevive;
				break;
			}
		} else {
			neText.text = "NOT ENOUGH DIAMONDS";
		}

		s2Panel.gameObject.SetActive (false);
		nePanel.gameObject.SetActive (true);
	}
}
