﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinLoader : MonoBehaviour {

	public Transform[] balls;
	public Transform mainBall;

	private int currentSkinIndex;

	void Awake(){
		currentSkinIndex = PlayerPrefs.GetInt ("CurrentSkin");
		Debug.Log ("curSkinIdx = " + currentSkinIndex);

		for (int i = 0; i < balls.Length; i++) {
			if (i == currentSkinIndex) {
				mainBall.GetComponent<MeshFilter> ().mesh = balls [i].GetComponent<MeshFilter> ().sharedMesh;
				mainBall.localScale = balls [i].transform.localScale / 100f * 0.7610368f;
				mainBall.GetComponent<MeshRenderer> ().materials = balls [i].GetComponent<MeshRenderer> ().sharedMaterials;
				mainBall.localRotation = balls [i].transform.localRotation;
				if (balls [i].GetComponent<BallVariables> ().useColorMod) {
					mainBall.GetComponent<MeshRenderer> ().material.SetColor ("_Color", balls [i].GetComponent<BallVariables> ().color);
				}
			}
		}
	}
}