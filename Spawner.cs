﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    private GameVariables gamevarscript;
   
	private int counterplanar=1;
	private int SkenarioNow = 0;
    private int VarianNow;
    private Transform[][] prefabtemp = new Transform[6][];

	public Transform[] prefabsForest;
	public Transform[] prefabsFantasy;
	public Transform[] prefabsCandy;
    public Transform[] prefabsRuin;
    public Transform[] prefabsDesert;
    public Transform[] prefabsOcean;

    public MeshRenderer planedecoy;
    public Texture[] texturedecoy;


    public int jumlahskenario;
    public int jumlahvarian;

    public Camera kamera;

	void Start(){

        gamevarscript = transform.GetComponent(typeof(GameVariables)) as GameVariables;

        prefabtemp [0] = prefabsForest;
        prefabtemp [1] = prefabsFantasy;
        prefabtemp [2] = prefabsCandy;
        prefabtemp [3] = prefabsRuin;
        prefabtemp [4] = prefabsDesert;
        prefabtemp [5] = prefabsOcean;
	}

	public void SpawnPlanar(){
		if (counterplanar <= 9) {
			counterplanar++; 
		}
		else {
            if (SkenarioNow < jumlahskenario-1){
                SkenarioNow++;
	
                if (SkenarioNow >= gamevarscript.unlockedSkenario){
                    gamevarscript.unlockedSkenario++;
                    Debug.Log("unlockedskenario = " + gamevarscript.unlockedSkenario);
                    gamevarscript.achievementscenario();
                }
			}
			else{
                SkenarioNow = 0;		
			}
			counterplanar = 1;

            if (SkenarioNow == 5)
            {
                kamera.clearFlags = CameraClearFlags.SolidColor;
            }
            else
                kamera.clearFlags = CameraClearFlags.Skybox;
            
		}

        VarianNow = Random.Range (0, jumlahvarian);

        Instantiate (prefabtemp[SkenarioNow][VarianNow]);
        planedecoy.material.mainTexture = texturedecoy[SkenarioNow];
        Debug.Log (SkenarioNow + "  ,  " + VarianNow + "  ,  " + counterplanar);
	}





}
