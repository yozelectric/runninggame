﻿using UnityEngine;
using System.Collections;

public class TouchControl : MonoBehaviour {

	[SerializeField]
	float _horizontallimit = 2.5f, _verticallimit = -2.5f, dragSpeed = 0.1f;

    private GameEventManager eventscr;
	public GameObject Player;
	Transform cachedTransform;
	Vector3 startingPos;


	// Use this for initialization
	void Start () 
	{
        eventscr = GameObject.FindGameObjectWithTag("Spawner").GetComponent(typeof(GameEventManager)) as GameEventManager;
		//referencia transform
		cachedTransform = Player.transform;

		//salva a posicao inicial
		startingPos = cachedTransform.position;
	}

	// Update is called once per frame
	void Update () 
	{
       
        if (!eventscr.gameOver)
        {
            if (!eventscr.pause)
            {
                if (!eventscr.tekscountdowngame.gameObject.activeInHierarchy)
                {
                    {
                        Vector3 CurrentPosition = this.transform.position;
                        float clampedx = Mathf.Clamp(CurrentPosition.x, -2.0f, 2.0f);
                        if (Input.touchCount > 0)
                        {
                            Vector2 deltaPosition = Input.GetTouch(0).deltaPosition;
                            switch (Input.GetTouch(0).phase)
                            {
                                case TouchPhase.Began:
                                    break;

                                case TouchPhase.Moved:
                                    cachedTransform.Translate(deltaPosition.x * 0.01f, 0, 0, Space.World);
                                    //DragObj(deltaPosition);
                                    break;

                                case TouchPhase.Ended:
                                    break;
                            }
                        }

                        if (!Mathf.Approximately(clampedx, CurrentPosition.x))
                        {
                            CurrentPosition.x = clampedx;
                            transform.position = CurrentPosition;
                        }
                    }
                }
            }
        }
	}

	void DragObj (Vector2 deltaPos)
	{
		cachedTransform.position = new Vector3(Mathf.Clamp((deltaPos.x * dragSpeed) + cachedTransform.position.x,
			startingPos.x - _horizontallimit, startingPos.x + _horizontallimit),
			Mathf.Clamp((deltaPos.y * dragSpeed) + cachedTransform.position.y,
				startingPos.y - _verticallimit, startingPos.y + _verticallimit),
			cachedTransform.position.z);
	}
}﻿