﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlscript : MonoBehaviour {

    private GameEventManager eventscr;
	// Use this for initialization
	void Start () {
        eventscr = GameObject.FindGameObjectWithTag("Spawner").GetComponent(typeof(GameEventManager)) as GameEventManager;

	}
	
	// Update is called once per frame
	void Update () {
        if (!eventscr.gameOver)
        {
            if (!eventscr.pause)
            {
                if (!eventscr.tekscountdowngame.gameObject.activeInHierarchy)
                {
                    {
                        Vector3 CurrentPosition = this.transform.position;
                        float clampedx = Mathf.Clamp(CurrentPosition.x, -2.0f, 2.0f);
                        if (Input.GetKey(KeyCode.LeftArrow))
                        {
                            transform.Translate(-0.08f, 0, 0);
                        }
                        if (Input.GetKey(KeyCode.RightArrow))
                        {
                            transform.Translate(0.08f, 0, 0);
                        }
                        if (!Mathf.Approximately(clampedx, CurrentPosition.x))
                        {
                            CurrentPosition.x = clampedx;
                            transform.position = CurrentPosition;
                        }
                    }
                } 
            }
        }
	}
}
