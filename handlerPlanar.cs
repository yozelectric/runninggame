﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class handlerPlanar : MonoBehaviour {

	private HandlerBola scriptbola;
    private GameEventManager eventscr;
	private Spawner scriptspawner;

	// Use this for initialization
	void Start () {
		scriptbola = GameObject.FindGameObjectWithTag ("Player").GetComponent (typeof(HandlerBola))as HandlerBola;
        eventscr = GameObject.FindGameObjectWithTag("Spawner").GetComponent(typeof(GameEventManager)) as GameEventManager;
		scriptspawner= GameObject.FindGameObjectWithTag ("Spawner").GetComponent (typeof(Spawner))as Spawner;
	}
		
	// Update is called once per frame
	void Update () {
        if (!eventscr.gameOver) {
            if (eventscr.pause)
                transform.Translate(0, 0, 0);
            else
            {
                if (!eventscr.tekscountdowngame.gameObject.activeInHierarchy)
                    transform.Translate(0, 0.01f * scriptbola.speed, 0);
            }
                
                
			if (transform.localPosition.z < -30) {
				scriptspawner.SpawnPlanar ();
				Destroy (this.gameObject);
			}
		}
	}
}
